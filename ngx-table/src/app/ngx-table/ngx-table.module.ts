import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxTableComponent } from './ngx-table/ngx-table.component';
import { FormsModule } from '@angular/forms';
import {
  NgxTableColumnCellTemplateDirective,
  NgxTableColumnComponent,
  NgxTableColumnFooterTemplateDirective,
  NgxTableColumnHeaderCellTemplateDirective
} from './ngx-table-template';
import { FindColumnTemplatePipe, ReduceValuePipe } from './pipes';
import { NgxTableService } from './services';

const COMPONENTS_EXPORT = [
  NgxTableComponent,
  NgxTableColumnComponent,
  NgxTableColumnCellTemplateDirective,
  NgxTableColumnHeaderCellTemplateDirective,
  NgxTableColumnFooterTemplateDirective,
];
const COMPONENTS_ENTRY = [];
const COMPONENTS = [...COMPONENTS_EXPORT, ...COMPONENTS_ENTRY,
  FindColumnTemplatePipe,
  ReduceValuePipe];

const MODULES_EXPORT = [];
const MODULES = [...MODULES_EXPORT, CommonModule, FormsModule];

@NgModule({
  declarations: [...COMPONENTS],
  entryComponents: [...COMPONENTS_ENTRY],
  imports: [...MODULES],
  exports: [...MODULES_EXPORT, ...COMPONENTS_EXPORT],
  providers: [NgxTableService]
})
export class NgxTableModule {
}
