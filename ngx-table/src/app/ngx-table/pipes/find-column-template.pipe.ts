import { Pipe, PipeTransform, QueryList } from '@angular/core';
import { NgxTableColumnComponent } from '../ngx-table-template';

@Pipe({
  name: 'findColumnTemplate'
})
export class FindColumnTemplatePipe implements PipeTransform {

  transform(field: string, columnTemplates: QueryList<NgxTableColumnComponent>, type: 'cell' | 'header' | 'footer'): any {
    const findObj = (columnTemplates || []).find(d => d.field === field);
    if (findObj) {
      switch (type) {
        case 'cell':
          if (!findObj.cellTemplate || (findObj.cellTemplate && !findObj.cellTemplate.templateRef)) {
            return null;
          }
          return findObj.cellTemplate.templateRef;
        case 'header':
          if (!findObj.headerTemplate || (findObj.headerTemplate && !findObj.headerTemplate.templateRef)) {
            return null;
          }
          return findObj.filterTemplate.templateRef;
        case 'footer':
          if (!findObj.footerTemplate || (findObj.footerTemplate && !findObj.footerTemplate.templateRef)) {
            return null;
          }
          return findObj.footerTemplate.templateRef;
      }
    }
    return null;
  }

}
