import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reduceValue'
})
export class ReduceValuePipe implements PipeTransform {

  transform(value: any, id: string): any {
    return this.resolveFieldData(value, id);
  }

  resolveFieldData(data: any, field: any): any {
    if (data && field) {
      if (field.indexOf('.') === -1) {
        return data[field];
      } else {
        const fields: string[] = field.split('.');
        let value = data;
        for (let i = 0, len = fields.length; i < len; ++i) {
          if (value == null) {
            return null;
          }
          value = value[fields[i]];
        }
        return value;
      }
    } else {
      return null;
    }
  }
}
