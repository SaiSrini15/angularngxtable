import { TestBed } from '@angular/core/testing';

import { NgxTableService } from './ngx-table.service';

describe('NgxTableService', () => {
  let service: NgxTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgxTableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return pagination details based on current index, total record and page size', () => {
    const details = service.getPaginationDetails(200, 1, 20);
    expect(details.totalItems).toBe(200);
    expect(details.currentPage).toBe(1);
    expect(details.pageSize).toBe(20);
    expect(details.totalPages).toBe(10);
    expect(details.startPage).toBe(1);
    expect(details.endPage).toBe(10);
    expect(details.startIndex).toBe(0);
    expect(details.endIndex).toBe(19);
    expect(details.pages).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  });

});
