export class NgxTablePagination {
  total = 0;
  pageSize = 10;
  pageIndex = 1;
  listOfOption?: number[] = [10, 20, 50, 100];
}

export class NgxTablePaginationDetails {
  totalItems: number;
  currentPage: number;
  pageSize: number;
  totalPages: number;
  startPage: number;
  endPage: number;
  startIndex: number;
  endIndex: number;
  pages: number[];
}
