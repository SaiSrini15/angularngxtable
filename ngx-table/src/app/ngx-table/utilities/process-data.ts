import { NgxTablePagination } from '../types';

export function processData(data: any[], pagination: NgxTablePagination): Array<any> {
  return data.slice((pagination.pageIndex - 1) * pagination.pageSize, pagination.pageIndex * pagination.pageSize);
}
