import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';

import { NgxTableComponent } from './ngx-table.component';
import { NgxTableService } from '../services/ngx-table.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

describe('NgxTableComponent', () => {
  let component: NgxTableComponent;
  let fixture: ComponentFixture<NgxTableComponent>;
  let injector: TestBed;
  let ngxTableService: NgxTableService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NgxTableComponent],
      imports: [CommonModule, FormsModule],
      providers: [NgxTableService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxTableComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    ngxTableService = injector.inject(NgxTableService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set table pagination', () => {
    component.pagination = {
      pageSize: 20,
      pageIndex: 3,
      total: 200
    };
    component.setPagination();
    expect(component.tablePaginationDetails.pageSize).toBe(component.pagination.pageSize);
    expect(component.tablePaginationDetails.currentPage).toBe(component.pagination.pageIndex);
    expect(component.tablePaginationDetails.totalItems).toBe(component.pagination.total);
  });

  it('should emit event when pageIndex changes and emit event', () => {
    spyOn(component.pageChange, 'emit');
    component.onPageChange(2);
    expect(component.tablePaginationDetails.currentPage).toBe(2);
    expect(component.pagination.pageIndex).toBe(2);
    expect(component.pageChange.emit).toHaveBeenCalled();
  });

  it('should emit event when page size changes and emit event', () => {
    spyOn(component.pageChange, 'emit');
    component.tablePaginationDetails.pageSize = 20;
    component.onPageSizeChange();
    expect(component.pagination.pageSize).toBe(20);
    expect(component.pageChange.emit).toHaveBeenCalled();
  });

});
