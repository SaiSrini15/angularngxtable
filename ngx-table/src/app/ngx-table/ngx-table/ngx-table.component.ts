import { Component, ContentChildren, EventEmitter, Input, Output, QueryList, ViewEncapsulation } from '@angular/core';
import { NgxTablePagination, NgxTablePaginationDetails } from '../types';
import { NgxTableColumnComponent } from '../ngx-table-template';
import { NgxTableService } from '../services';

@Component({
  selector: 'ngx-table',
  templateUrl: './ngx-table.component.html',
  styleUrls: ['./ngx-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NgxTableComponent {

  private _dataSource: any[] = [];

  get dataSource(): any[] {
    return this._dataSource;
  }

  @Input('dataSource')
  set dataSource(value: any[]) {
    this._dataSource = value;
    this.setPagination();
  }

  private _pagination: NgxTablePagination = new NgxTablePagination();
  public tablePaginationDetails: NgxTablePaginationDetails = new NgxTablePaginationDetails();

  @Input('pagination')
  set pagination(value: NgxTablePagination) {
    this._pagination.pageIndex = value.pageIndex || 1;
    this._pagination.pageSize = value.pageSize || 10;
    this._pagination.total = value.total || 0;
    if (value.listOfOption) {
      this._pagination.listOfOption = value.listOfOption;
    }
    this.setPagination();
  }

  get pagination(): NgxTablePagination {
    return this._pagination;
  }

  @Input() tableHeight = 400;
  @Input() showFooter = false;
  @Input() showPagination = true;

  @ContentChildren(NgxTableColumnComponent, {descendants: true}) columnTemplates: QueryList<NgxTableColumnComponent>;

  @Output()
  pageChange: EventEmitter<NgxTablePagination> = new EventEmitter<NgxTablePagination>();

  constructor(public ngxTableService: NgxTableService) {
  }

  /**
   * to set and calculate pagination details based on pageIndex, pageSize and totalRecords.
   */
  setPagination() {
    this.tablePaginationDetails = this.ngxTableService.getPaginationDetails(
      this.pagination.total,
      this.pagination.pageIndex,
      this.pagination.pageSize
    );
  }

  /**
   * To set pageIndex to current page for tablePagination details and emit pageChange eventEmitter
   */
  onPageChange(pageIndex: number): void {
    this.tablePaginationDetails.currentPage = pageIndex;
    this.pagination.pageIndex = pageIndex;
    this.pageChange.emit(this.pagination);
  }

  /**
   * Used when page size changes from pagination selection options
   */
  onPageSizeChange(): void {
    this.pagination.pageSize = this.tablePaginationDetails.pageSize;
    this.pagination.pageIndex = 1;
    this.pageChange.emit(this.pagination);
  }
}
