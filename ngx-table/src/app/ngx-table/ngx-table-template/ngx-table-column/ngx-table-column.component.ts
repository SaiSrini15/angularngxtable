import { Component, ContentChild, Directive, Input, OnInit, TemplateRef } from '@angular/core';

@Directive({
    selector: 'ng-template[ngxTableColumnHeaderCellTemplate]'
})
export class NgxTableColumnHeaderCellTemplateDirective {
    constructor(public templateRef: TemplateRef<NgxTableColumnHeaderCellTemplateDirective>) {
    }
}

@Directive({
    selector: 'ng-template[ngxTableColumnCellTemplate]'
})
export class NgxTableColumnCellTemplateDirective {
    constructor(public templateRef: TemplateRef<NgxTableColumnCellTemplateDirective>) {
    }
}

@Directive({
    selector: 'ng-template[ngxTableColumnFooterTemplate]'
})
export class NgxTableColumnFooterTemplateDirective {
    constructor(public templateRef: TemplateRef<NgxTableColumnFooterTemplateDirective>) {
    }
}

@Component({
    selector: 'ngx-table-column',
    template: ``
})
export class NgxTableColumnComponent implements OnInit {

    @Input() field: string;
    @Input() title: string;
    @Input() width = '100%';


    @ContentChild(NgxTableColumnHeaderCellTemplateDirective, {static: false})
    headerTemplate: TemplateRef<NgxTableColumnHeaderCellTemplateDirective>;

    @ContentChild(NgxTableColumnCellTemplateDirective, {static: false})
    cellTemplate: TemplateRef<NgxTableColumnCellTemplateDirective>;

    @ContentChild(NgxTableColumnFooterTemplateDirective, {static: false})
    footerTemplate: TemplateRef<NgxTableColumnFooterTemplateDirective>;

    constructor() {
    }

    ngOnInit(): void {
    }

}
