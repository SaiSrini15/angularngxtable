import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxTableColumnComponent } from './ngx-table-column.component';

describe('NgxTableColumnComponent', () => {
  let component: NgxTableColumnComponent;
  let fixture: ComponentFixture<NgxTableColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgxTableColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxTableColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
