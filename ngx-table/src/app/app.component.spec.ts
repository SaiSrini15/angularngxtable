import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { NgxTableModule } from './ngx-table/ngx-table.module';
import { NgxTablePagination } from './ngx-table/types';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        NgxTableModule,
        HttpClientTestingModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    httpMock = injector.inject(HttpTestingController);
    fixture.detectChanges();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should create the app', () => {
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ngx-table'`, () => {
    const app = fixture.componentInstance;
    expect(app.title).toEqual('ngx-table');
  });

  it('should render title', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('ngx-table');
  });

  it('should set table datasource for pagination', () => {
    component.setTableData();
    expect(component.dataSource.length).toBe(10);
  });

  it('should set pageIndex and pageSize onPageChange', () => {
    spyOn(component, 'setTableData');
    expect(component.pagination.pageIndex).toBe(1);
    expect(component.pagination.pageSize).toBe(10);
    const pagination: NgxTablePagination = {
      total: 200,
      pageSize: 20,
      pageIndex: 2
    };
    component.onPageChange(pagination);
    expect(component.pagination.pageIndex).toBe(2);
    expect(component.pagination.pageSize).toBe(20);
    expect(component.setTableData).toHaveBeenCalled();
  });

  it('should call http client post method', () => {
    const url = '/api/submit';
    const dataItem = {
      id: 10
    };
    component.onSubmitClick(dataItem);
    const req = httpMock.expectOne(url);
    httpMock.verify();
    expect(req.request.method).toBe('POST');
  });

});
