import { Component, OnInit } from '@angular/core';
import { NgxTablePagination } from './ngx-table/types';
import { processData } from './ngx-table/utilities';
import { tableData } from '../assets/fake-data';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ngx-table';
  pagination: NgxTablePagination = {
    pageIndex: 1,
    total: 200,
    pageSize: 10
  };
  data = [];
  dataSource: any[] = [];

  constructor(public httpClient: HttpClient) {
    this.data = tableData.map((o, index) => {
      o.srNo = index + 1;
      return o;
    });
  }

  ngOnInit(): void {
    this.setTableData();
  }

  setTableData(): void {
    this.dataSource = processData(this.data, this.pagination);
  }

  onPageChange(event: NgxTablePagination): void {
    this.pagination.pageIndex = event.pageIndex;
    this.pagination.pageSize = event.pageSize;
    this.setTableData();
  }

  onSubmitClick(dataItem: any): void {
    const url = '/api/submit';
    const body = JSON.stringify({rowId: dataItem.id});
    const header = {headers: {'content-type': 'application/json'}};
    this.httpClient.post(url, body, header)
      .subscribe((response) => {
        console.log(response);
      });
  }
}
